package com.grupodiagnosticoaries.demografico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

//@EnableResourceServer
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class DemograficoApp extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(DemograficoApp.class, args);
    }
    
    @Override
    protected SpringApplicationBuilder configure (SpringApplicationBuilder builder) {
    	return builder.sources(DemograficoApp.class);
    }

//    @EnableGlobalMethodSecurity(prePostEnabled = true)
//    protected static class GlobalSecurityConfiguration extends GlobalMethodSecurityConfiguration {
//        @Override
//        protected MethodSecurityExpressionHandler createExpressionHandler() {
//            return new OAuth2MethodSecurityExpressionHandler();
//        }
//    }
}