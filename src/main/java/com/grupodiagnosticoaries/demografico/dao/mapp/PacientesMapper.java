package com.grupodiagnosticoaries.demografico.dao.mapp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.grupodiagnosticoaries.demografico.dto.PacientesDto;

public class PacientesMapper implements RowMapper<PacientesDto>{

	@Override
	public PacientesDto mapRow(ResultSet rs, int arg1) throws SQLException {
		PacientesDto dto = new PacientesDto();
		dto.setConsecutivo(rs.getInt(1));
		dto.setSsucursal(rs.getString(2));
		dto.setUorden(rs.getInt(3));
		dto.setSnombresucursal(rs.getString(4));
		dto.setPaciente(rs.getString(5));
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date dnacimiento = rs.getDate(6);
		dto.setDnacimiento(sdf.format(dnacimiento));
		return dto;
	}

}
