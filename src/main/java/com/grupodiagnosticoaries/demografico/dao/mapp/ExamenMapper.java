package com.grupodiagnosticoaries.demografico.dao.mapp;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.grupodiagnosticoaries.demografico.dto.ExamenDto;

public class ExamenMapper implements RowMapper<ExamenDto> {

	@Override
	public ExamenDto mapRow(ResultSet rs, int arg) throws SQLException {

		ExamenDto examenDto = new ExamenDto(
				rs.getInt(1),
				rs.getString(2).replaceAll("\"", ""),
				rs.getString(3),
				rs.getInt(4));
		
		return examenDto;
	}

}
