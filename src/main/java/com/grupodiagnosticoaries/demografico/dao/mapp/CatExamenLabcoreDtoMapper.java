package com.grupodiagnosticoaries.demografico.dao.mapp;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.grupodiagnosticoaries.demografico.dto.CatExamenLabcoreDto;


public class CatExamenLabcoreDtoMapper implements RowMapper<CatExamenLabcoreDto> {

	@Override
	public CatExamenLabcoreDto mapRow(ResultSet rs, int arg) throws SQLException {

		CatExamenLabcoreDto catExamenLabcoreDto = new CatExamenLabcoreDto(
				rs.getInt(1),
				rs.getString(2),
				rs.getInt(3),
				rs.getInt(4)
				);
		
		return catExamenLabcoreDto;
	}

}
