package com.grupodiagnosticoaries.demografico.dao.mapp;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.grupodiagnosticoaries.demografico.dto.PacienteInfoDto;

public class InfoPacienteMapper implements RowMapper<PacienteInfoDto>{

	@Override
	public PacienteInfoDto mapRow(ResultSet rs, int arg1) throws SQLException {
		PacienteInfoDto dto = new PacienteInfoDto();
		dto.setPaciente(rs.getString(1));
		dto.setTelefono(rs.getString(2));
		dto.setSconvenio(rs.getString(3));
		return dto;
	}

}
