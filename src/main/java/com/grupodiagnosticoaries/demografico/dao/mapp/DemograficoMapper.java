package com.grupodiagnosticoaries.demografico.dao.mapp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.grupodiagnosticoaries.demografico.dto.DemograficoDto;

public class DemograficoMapper implements RowMapper<DemograficoDto> {

	@Override
	public DemograficoDto mapRow(ResultSet rs, int arg) throws SQLException {
//		CMarcaDto cMarcaDto = new CMarcaDto();
//		cMarcaDto.setcMarca(rs.getInt("cmarca"));
//		cMarcaDto.setsMarca(rs.getString("smarca"));
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		DemograficoDto dto = new DemograficoDto(
				rs.getString(1), 
				rs.getString(2), 
				rs.getString(3), 
				rs.getString(4), 
				rs.getInt(5), 
				rs.getInt(6), 
				rs.getString(7), 
				rs.getString(8), 
				sdf.format(rs.getDate(9)), 
				rs.getString(10), 
				rs.getString(11), 
				rs.getInt(12), 
				rs.getString(13), 
				rs.getInt(14), 
				rs.getString(15), 
				rs.getInt(16), 
				rs.getString(17), 
				rs.getString(18), 
				rs.getString(19), 
				rs.getString(20),
				rs.getInt(21));
		return dto;
	}

}
