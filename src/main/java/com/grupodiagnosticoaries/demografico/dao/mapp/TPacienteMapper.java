package com.grupodiagnosticoaries.demografico.dao.mapp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.RowMapper;

import com.grupodiagnosticoaries.demografico.dto.TPacienteDto;

public class TPacienteMapper implements RowMapper<TPacienteDto>{

	@Override
	public TPacienteDto mapRow(ResultSet rs, int arg1) throws SQLException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		
		TPacienteDto dto = new TPacienteDto(
				rs.getInt("kpaciente"), 
				rs.getString("snombre"), 
				rs.getString("sapellidopaterno"), 
				rs.getString("sapellidomaterno"), 
				sdf.format(rs.getDate("dnacimiento")));
		return dto;
	}

}
