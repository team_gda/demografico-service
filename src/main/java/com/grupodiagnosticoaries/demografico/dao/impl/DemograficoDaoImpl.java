package com.grupodiagnosticoaries.demografico.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.grupodiagnosticoaries.demografico.dao.inte.IDemograficoDao;
import com.grupodiagnosticoaries.demografico.dao.mapp.DemograficoMapper;
import com.grupodiagnosticoaries.demografico.dao.mapp.ExamenMapper;
import com.grupodiagnosticoaries.demografico.dao.mapp.InfoPacienteMapper;
import com.grupodiagnosticoaries.demografico.dao.mapp.PacientesMapper;
import com.grupodiagnosticoaries.demografico.dao.mapp.TPacienteMapper;
import com.grupodiagnosticoaries.demografico.dto.DemograficoDto;
import com.grupodiagnosticoaries.demografico.dto.ExamenDto;
import com.grupodiagnosticoaries.demografico.dto.FilterPacienteDto;
import com.grupodiagnosticoaries.demografico.dto.PacienteInfoDto;
import com.grupodiagnosticoaries.demografico.dto.PacientesDto;
import com.grupodiagnosticoaries.demografico.dto.TPacienteDto;

@Repository("demograficoDao")
public class DemograficoDaoImpl  extends JdbcDaoSupport implements IDemograficoDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public DemograficoDto consultarDemograficoByOrden(Integer kordensucursal) {
		
		logger.debug("ejecutando consultarDemograficoByOrden[" + kordensucursal +"]");
		
		DemograficoDto demograficoDto;
		String query  = "select tp.snombre,tp.sapellidopaterno,tp.sapellidomaterno, tp.sdireccion ||' '|| cp.scolonia ||' Col. '|| cp.sdelegacionmunicipio  ||' '|| cp.cpostal sdireccion, " + 
				" tp.ccodigopostal, tp.bsexo, tp.stelefono, tp.scorreoelectronico, " + 
				" tp.dnacimiento, get_edad(to_char(tp.dnacimiento,'yyyy-mm-dd')) edad, cs.ssucursal, cs.csucursal," + 
				" cs.sdireccion || ' ' || cps.sasentamiento || ' '|| cps.scolonia || ' '|| " + 
				" cps.cpostal || ' '|| cps.sdelegacionmunicipio || ' '|| cps.sestado sdireccionsucursal, tos.kordensucursal," +
				" cc.sconvenio , tos.cmarca, to_char(tos.dregistro,'yyyy-MM-dd hh24:mm'), cs.snombresucursal, cm.sapellidopaterno || ' ' || cm.sapellidomaterno || ' ' || cm.snombre  nombre_medico, " +
				" cs.stelefono stelefono_sucursal, tp.kpaciente  " + 
				" from t_orden_sucursal tos " + 
				" inner join t_paciente tp on tos.kpaciente = tp.kpaciente " + 
				" inner join c_sucursal cs on tos.csucursal = cs.csucursal " + 
				" inner join c_convenio cc on tos.cconvenio = cc.cconvenio " + 
				" inner join c_medico cm on tos.cmedico = cm.cmedico " +
				" inner join c_codigo_postal cp on tp.ccodigopostal = cp.ccodigopostal " + 
				" inner join c_codigo_postal cps on cps.ccodigopostal = cs.ccodigopostal " +
				" where tos.kordensucursal = ? ";
		demograficoDto = this.getJdbcTemplate().queryForObject(query, new Object[] {kordensucursal}, new DemograficoMapper());
		
		logger.debug("ejecutando consultarDemograficoByOrden ejecutado:");
		
		return demograficoDto;
	}
	
	@Override
	public List<ExamenDto> selectAllExamenesByOrden(Integer kOrdenSucursal) {
		
		logger.debug("ejecutando selectAllExamenesByOrden[" + kOrdenSucursal +"]");
		
		List<ExamenDto> listExamenDto;
		String query  = "select ce.cexamen, ce.sexamen, ce.scodigolis, ce.cexamenproceso from web2lablis.c_examen ce " + 
				"inner join t_orden_examen_sucursal toes on toes.cexamen = ce.cexamen " + 
				"inner join t_orden_sucursal tos on tos.kordensucursal = toes.kordensucursal  " + 
				"where  tos.kordensucursal = ? ";
		listExamenDto = this.getJdbcTemplate().query(query, new Object[] {kOrdenSucursal}, new ExamenMapper());
		
		logger.debug("ejecutando selectAllExamenesByOrden ejecutado:");
		
		return listExamenDto;
	}
	
	@Override
	public String consultarNombreExamenProcesoById(Integer id){
		logger.debug("ejecutando consultarNombreExamenProcesoById[" + id +"]");
		
		String nombreExamen;
		String query  = "select sexamen from web2lablis.c_examen where cexamenproceso = ? ";
		nombreExamen = this.getJdbcTemplate().queryForObject(query, new Object[] {id}, String.class);
		
		logger.debug("consultarNombreExamenProcesoById ejecutado:");
		
		return nombreExamen;
	}
	
	@Override
	public List<ExamenDto> selectAllExamenesByCotizacion(Integer cotizacion) {
		
		logger.debug("ejecutando selectAllExamenesByCotizacion[" + cotizacion +"]");
		
		List<ExamenDto> listExamenDto;
		String query  = " select  toesc.cexamen, toesc.sexamen, ce.scodigolis , ce.cexamenproceso" +
				" from t_orden_sucursal_cotizacion tos  " +
				" inner join t_orden_examen_sucursal_cotizacion toesc on toesc.kordensucursalcotizacion =  tos.kordensucursalcotizacion " +
				" inner join web2lablis.c_examen ce on toesc.cexamen =ce.cexamen " +
				" where tos.kordensucursalcotizacion = ? " ;
		listExamenDto = this.getJdbcTemplate().query(query, new Object[] {cotizacion}, new ExamenMapper());
		
		logger.debug("ejecutando selectAllExamenesByCotizacion ejecutado:");
		
		return listExamenDto;
	}
	
//	@Override
//	public TPacienteDto consultarDemograficoByCotizacion(Integer cotizacion){
//		logger.debug("ejecutando consultarDemograficoByCotizacion[" + cotizacion +"]");
//		
//		TPacienteDto dto;
//		String query  = "select tp.*  " +
//				"from t_orden_sucursal_cotizacion tc   " +
//				"inner join t_paciente tp on tc.kpaciente =  " +
//				"tp.kpaciente where kordensucursalcotizacion = ? " ;
//		
//		dto = this.getJdbcTemplate().queryForObject(query, new Object[] {cotizacion}, new TPacienteMapper());
//		
//		logger.debug("ejecutando consultarDemografico ejecutado:");
//		
//		return dto;
//	}
	
	@Override
	public DemograficoDto consultarDemograficoByCotizacion(Integer cotizacion) {
		
		logger.debug("ejecutando consultarDemograficoByCotizacion[" + cotizacion +"]");
		
		DemograficoDto demograficoDto;
		String query  = " select tp.snombre,tp.sapellidopaterno,tp.sapellidomaterno, tp.sdireccion ||' '|| cp.scolonia ||' Col. '|| cp.sdelegacionmunicipio  ||' '|| cp.cpostal sdireccion,  " +
				" tp.ccodigopostal, tp.bsexo, tp.stelefono, tp.scorreoelectronico,  " +
				" tp.dnacimiento, get_edad(to_char(tp.dnacimiento,'yyyy-mm-dd')) edad, cs.ssucursal, tos.csucursaldestino,  " +
				" cs.sdireccion || ' ' || cps.sasentamiento || ' '|| cps.scolonia || ' '||  " +
				" cps.cpostal || ' '|| cps.sdelegacionmunicipio || ' '|| cps.sestado sdireccionsucursal, tos.kordensucursalcotizacion, " +
				" cc.sconvenio , tos.cmarca, to_char(tos.dregistro,'yyyy-MM-dd hh24:mm'), cs.snombresucursal, cm.sapellidopaterno || ' ' || cm.sapellidomaterno || ' ' || cm.snombre  nombre_medico,  " +
				" cs.stelefono stelefono_sucursal, tp.kpaciente  " +
				" from t_orden_sucursal_cotizacion tos  " +
				" inner join t_paciente tp on tos.kpaciente = tp.kpaciente  " +
				" inner join c_sucursal cs on tos.csucursal = cs.csucursal  " +
				" inner join c_convenio cc on tos.cconvenio = cc.cconvenio  " +
				" inner join c_medico cm on tos.cmedico = cm.cmedico  " +
				" inner join c_codigo_postal cp on tp.ccodigopostal = cp.ccodigopostal  " +
				" inner join c_codigo_postal cps on cps.ccodigopostal = cs.ccodigopostal  " +
				" where tos.kordensucursalcotizacion = ? " ;
		demograficoDto = this.getJdbcTemplate().queryForObject(query, new Object[] {cotizacion}, new DemograficoMapper());
		
		logger.debug("ejecutando consultarDemograficoByCotizacion ejecutado:");
		
		return demograficoDto;
	}
	
	@Override
	public String consultarNombrePacienteById(Integer id){
		logger.debug("ejecutando consultarNombrePacienteById[" + id +"]");
		
		String nombrePaciente;
		String query  = "select snombre||' '||sapellidopaterno||' '||sapellidomaterno from t_paciente where kpaciente = ? ";
		nombrePaciente = this.getJdbcTemplate().queryForObject(query, new Object[] {id}, String.class);
		
		logger.debug("consultarNombrePacienteById ejecutado:");
		
		return nombrePaciente;
	}
	
	@Override
	public String consultarFechaCotizacion(Integer id){
		logger.debug("ejecutando consultarFechaCotizacion[" + id +"]");
		String fecha;
		
		String query = "select dregistro from t_orden_sucursal_cotizacion where kordensucursalcotizacion = ? ";
		
		fecha = this.getJdbcTemplate().queryForObject(query, new Object[] {id}, String.class);
		
		logger.debug("consultarFechaCotizacion ejecutado:");
		return fecha;
	}
	
	
	@Override
	public Integer consultarEstatusExamenByConsecutivoAndExamen(Integer consecutivo, Integer examen){
		logger.debug("ejecutando consultarEstatusExamenByConsecutivoAndExamen[" + consecutivo +", "+examen+"]");
		
		Integer estatus;
		String query  = "select tms.cestadoregistro " +
				"from t_orden_sucursal tos " +
				"inner join t_orden_examen_sucursal toes on tos.kordensucursal=toes.kordensucursal " +
				"inner join web2lablis.c_examen ce on toes.cexamen=ce.cexamen  " +
				"inner join r_examen_muestra_sucursal rems on rems.kordenexamensucursal=toes.kordenexamensucursal " +
				"inner join t_muestra_sucursal tms on tms.kmuestrasucursal=rems.kmuestrasucursal " +
				"where tos.kordensucursal = ? " +
				"and ce.cexamenproceso = ? " +
				"order by tos.kordensucursal; " ;
		
		estatus = this.getJdbcTemplate().queryForObject(query, new Object[] {consecutivo,examen}, Integer.class);
		
		logger.debug("consultarEstatusExamenByConsecutivoAndExamen ejecutado:");
		
		return estatus;
	}
	
	
	
	
	@Override
	public Date consultarNacimientoPacienteById(Integer id){
		logger.debug("ejecutando consultarNacimientoPacienteById[" + id +"]");
		
		Date nacimientoPaciente;
		String query  = "select dnacimiento from t_paciente where kpaciente = ? ";
		nacimientoPaciente = this.getJdbcTemplate().queryForObject(query, new Object[] {id}, Date.class);
		
		logger.debug("consultarNacimientoPacienteById ejecutado:");
		
		return nacimientoPaciente;
	}
	
	@Override
	public PacienteInfoDto consultarInfoPacienteByCotizacion(Integer id){
		logger.debug("ejecutando consultarInfoPacienteByCotizacion[" + id +"]");
		
		PacienteInfoDto info;
		String query  = "select tp.snombre||' '||tp.sapellidopaterno||' '||tp.sapellidomaterno nombre, tp.stelefono, cc.sconvenio   " +
				"from t_orden_sucursal_cotizacion tos   " +
				"inner join t_orden_Examen_sucursal_cotizacion toesc on toesc.kordensucursalcotizacion=tos.kordensucursalcotizacion " +
				"inner join t_paciente tp on tp.kpaciente = tos.kpaciente    " +
				"inner join c_convenio cc on cc.cconvenio = toesc.cconvenio   " +
				"where tos.kordensucursalcotizacion = ? limit 1 " ;
		info = this.getJdbcTemplate().queryForObject(query, new Object[] {id}, new InfoPacienteMapper());
		
		logger.debug("consultarInfoPacienteByCotizacion ejecutado:");
		
		return info;
	}
	
	@Override
	public PacienteInfoDto consultarInfoPacienteByConsecutivo(Integer id){
		logger.debug("ejecutando consultarInfoPacienteByConsecutivo[" + id +"]");
		
		PacienteInfoDto info;
		String query  = "select tp.snombre||' '||tp.sapellidopaterno||' '||tp.sapellidomaterno nombre, tp.stelefono, cc.sconvenio     " +
				"from t_orden_sucursal tos   " +
				"inner join t_paciente tp on tos.kpaciente = tp.kpaciente   " +
				"inner join t_orden_examen_sucursal toes on toes.kordensucursal = tos.kordensucursal " +
				"inner join c_convenio cc on toes.cconvenio = cc.cconvenio   " +
				"where tos.kordensucursal = ? limit 1 " ;
		info = this.getJdbcTemplate().queryForObject(query, new Object[] {id}, new InfoPacienteMapper());
		
		logger.debug("consultarInfoPacienteByConsecutivo ejecutado:");
		
		return info;
	}
	
	@Override
	public String consultarIndicacionesExamen(Integer examen){
		logger.debug("ejecutando consultarIndicacionesExamen[" + examen +"]");
		String indicaciones ;
		String query = "select scondicionpreanalitica from web2lablis.e_examen_configuracion where cexamen = ? ";
		
		indicaciones = this.getJdbcTemplate().queryForObject(query,  new Object[] {examen}, String.class);
		logger.debug("consultarIndicacionesExamen ejecutado");
		return indicaciones;
	}
	
	@Override
	public List<PacientesDto> consultarPacientes(FilterPacienteDto dto){
		logger.debug("ejecutando consultarPacientes[" + dto.toString() +"]");
		List<PacientesDto> list;
		String query = "select kordensucursal consecutivo, " +
				"	tos.ssucursal, uorden,  " +
				"	snombresucursal,  " +
				"	snombre||' '||sapellidopaterno||' '||sapellidomaterno nombre_paciente, " +
				"	dnacimiento " +
				"from t_orden_sucursal tos " +
				"inner join t_paciente tp on tp.kpaciente=tos.kpaciente " +
				"inner join c_sucursal cs on cs.csucursal=tos.csucursal " +
				"where snombre like '%"+dto.getNombre()+"%' " +
				"and sapellidopaterno like '%"+dto.getApPaterno()+"%' " +
				"and sapellidomaterno like '%"+dto.getApMaterno()+"%' " +
				"and tos.csucursal<>100 " ;
		list = this.getJdbcTemplate().query(query,  new Object[] {}, new PacientesMapper());
		logger.debug("consultarPacientes ejecutado [" + list.size() +"]");
		return list;
	}
	
	@Override
	public List<Integer> consultarKPacientesByFilter(FilterPacienteDto dto){
		logger.debug("ejecutando consultarKPacientesByFilter[" + dto.toString() +"]");
		List<Integer> list;
		String query = "select kpaciente " +
				"from t_paciente " +
				"where snombre like '%"+dto.getNombre()+"%' " +
				"and sapellidopaterno like '%"+dto.getApPaterno()+"%' " +
				"and sapellidomaterno like '%"+dto.getApMaterno()+"%' " ;
		list = this.getJdbcTemplate().queryForList(query,  new Object[] {}, Integer.class);
		logger.debug("consultarKPacientesByFilter ejecutado [" + list.size() +"]");
		return list;
	}
	
	@Override
	public String consultarUser(Integer idUser){
		logger.debug("ejecutando consultarUser[" + idUser +"]");
		String user = null;
		String query = "select first_name||' '||last_name nombre from turbine_user where user_id = ?";
		try {
			user = this.getJdbcTemplate().queryForObject(query,  new Object[] {idUser}, String.class);			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		String nombreUser = user !=null ? user : "";
		
		
		logger.debug("consultarUser ejecutado");
		return nombreUser;
	}
	
}
