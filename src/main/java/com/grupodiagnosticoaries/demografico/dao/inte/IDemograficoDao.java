package com.grupodiagnosticoaries.demografico.dao.inte;

import java.util.Date;
import java.util.List;

import com.grupodiagnosticoaries.demografico.dto.DemograficoDto;
import com.grupodiagnosticoaries.demografico.dto.ExamenDto;
import com.grupodiagnosticoaries.demografico.dto.FilterPacienteDto;
import com.grupodiagnosticoaries.demografico.dto.PacienteInfoDto;
import com.grupodiagnosticoaries.demografico.dto.PacientesDto;
import com.grupodiagnosticoaries.demografico.dto.TPacienteDto;

public interface IDemograficoDao {

	public List<ExamenDto> selectAllExamenesByOrden(Integer kOrdenSucursal);
	DemograficoDto consultarDemograficoByCotizacion(Integer cotizacion);
	DemograficoDto consultarDemograficoByOrden(Integer kordensucursal);
	List<ExamenDto> selectAllExamenesByCotizacion(Integer cotizacion);
	String consultarNombrePacienteById(Integer id);
	String consultarIndicacionesExamen(Integer examen);
	String consultarNombreExamenProcesoById(Integer id);
	List<PacientesDto> consultarPacientes(FilterPacienteDto dto);
	PacienteInfoDto consultarInfoPacienteByCotizacion(Integer id);
	PacienteInfoDto consultarInfoPacienteByConsecutivo(Integer id);
	String consultarUser(Integer idUser);
	List<Integer> consultarKPacientesByFilter(FilterPacienteDto dto);
	Date consultarNacimientoPacienteById(Integer id);
	Integer consultarEstatusExamenByConsecutivoAndExamen(Integer consecutivo, Integer examen);
	String consultarFechaCotizacion(Integer id);
	
}
