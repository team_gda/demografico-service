package com.grupodiagnosticoaries.demografico.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.grupodiagnosticoaries.demografico.bo.DemograficoBo;
import com.grupodiagnosticoaries.demografico.dto.DemograficoDto;
import com.grupodiagnosticoaries.demografico.dto.ExamenDto;
import com.grupodiagnosticoaries.demografico.dto.FilterPacienteDto;
import com.grupodiagnosticoaries.demografico.dto.PacienteInfoDto;
import com.grupodiagnosticoaries.demografico.dto.OrdenMarcaDto;
import com.grupodiagnosticoaries.demografico.dto.TPacienteDto;
import com.grupodiagnosticoaries.demografico.responses.ResponseDemograficoException;

@RestController
public class DemograficoController {
	final static Logger logger = LogManager.getLogger(DemograficoController.class);

	@Autowired
	private DemograficoBo demograficoBo;
	private Gson gson = new Gson();

	@PostMapping("/demografico-orden")
	public ResponseEntity<String> consultarDemograficoByOrden(@RequestBody Integer kOrdenSucursal) {
		try {
			
			DemograficoDto demograficoDto;
			
			demograficoDto = demograficoBo.consultarDemografico(kOrdenSucursal);
			List<ExamenDto> listExamenDto = demograficoBo.selectAllExamenesByOrden(kOrdenSucursal);
			demograficoDto.setListExamenes(listExamenDto);
			
			String sDemografico = gson.toJson(demograficoDto);
			
			return new ResponseEntity<String>(sDemografico, HttpStatus.CREATED);
			
		} catch (ResponseDemograficoException e) {
			logger.error("Error al timbrar: " + e.getResponseTimbradoDto().toString());
			return new ResponseEntity<String>(gson.toJson(e.getResponseTimbradoDto()), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	@PostMapping("/demografico-cotizacion")
	public ResponseEntity<String> consultarDemograficoByCotizacion(@RequestBody Integer cotizacion) {
		try {
			DemograficoDto demograficoDto;
			demograficoDto = demograficoBo.consultaDemograficoByCotizacion(cotizacion);
			List<ExamenDto> listExamenDto = demograficoBo.selectAllExamenesByCotizacion(cotizacion);
			demograficoDto.setListExamenes(listExamenDto);

			String sDemografico = gson.toJson(demograficoDto);
			
			return new ResponseEntity<String>(sDemografico, HttpStatus.CREATED);
			
		} catch (ResponseDemograficoException e) {
			logger.error("Error al timbrar: " + e.getResponseTimbradoDto().toString());
			return new ResponseEntity<String>(gson.toJson(e.getResponseTimbradoDto()), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	@PostMapping("/get-dregistro-cotizacion")
	public ResponseEntity<String> consultarFechaRegistroCotizacion(@RequestBody Integer id){
		try {			
			return new ResponseEntity<String>(demograficoBo.consultarFechaRegistroCotizacion(id), HttpStatus.CREATED);			
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	
	
	@PostMapping("/nombrepaciente-id")
	public ResponseEntity<String> consultarNombrePacienteById(@RequestBody Integer id) {
		try {
			
			return new ResponseEntity<String>(demograficoBo.consultarNombrePacienteById(id), HttpStatus.CREATED);
			
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	
	
	@PostMapping("/estatus-examen-gda/{consecutivo}/{examen}")
	public ResponseEntity<String> consultarEstatusExamenGda(@PathVariable Integer consecutivo,@PathVariable Integer examen) {
		try {
			
			return new ResponseEntity<String>(demograficoBo.consultarEstatusExamen(consecutivo,examen).toString(), HttpStatus.CREATED);
			
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	
	
	
	@PostMapping("/nacimientopaciente-id")
	public ResponseEntity<String> consultarNacimientoPacienteById(@RequestBody Integer id) {
		try {
			
			return new ResponseEntity<String>(demograficoBo.consultarNacimientoPacienteById(id), HttpStatus.CREATED);
			
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	@PostMapping("/nombre-examen-proceso-id")
	public ResponseEntity<String> consultarNombreExamenProcesoById(@RequestBody Integer id) {
		try {
			
			return new ResponseEntity<String>(demograficoBo.consultarNombreExamenProcesoById(id), HttpStatus.CREATED);
			
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	@PostMapping("/indicaciones-id")
	public ResponseEntity<String> consultarIndicacionesByExamen(@RequestBody Integer id) {
		try {
			
			return new ResponseEntity<String>(demograficoBo.consultarIndicacionesByExamen(id), HttpStatus.CREATED);
			
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	@PostMapping("/find-pacientes")
	public @ResponseBody String findPacientes(@RequestBody FilterPacienteDto dto){
		return gson.toJson(demograficoBo.getListPacientes(dto));
	}
	
	@PostMapping("/nombrepacienteandtelefono-id")
	public ResponseEntity<String> consultarNombrePacienteAndTelefonoById(@RequestBody Integer id) {
		try {
			
			return new ResponseEntity<String>(demograficoBo.consultarNombrePacienteById(id), HttpStatus.CREATED);
			
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	
	
	@PostMapping("/info-paciente-cotizacion")
	public ResponseEntity<String> infoPacienteByCotizacion(@RequestBody Integer cotizacion) {
		try {
			PacienteInfoDto info = demograficoBo.getInfoPacienteCotizacion(cotizacion);
			String sDemografico = gson.toJson(info);			
			return new ResponseEntity<String>(sDemografico, HttpStatus.CREATED);			
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	
	@PostMapping("/info-paciente-consecutivo")
	public ResponseEntity<String> infoPacienteByConsecutivo(@RequestBody Integer consecutivo) {
		try {
			PacienteInfoDto info = demograficoBo.getInfoPacienteConsecutivo(consecutivo);
			String sDemografico = gson.toJson(info);			
			return new ResponseEntity<String>(sDemografico, HttpStatus.CREATED);			
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	
	@PostMapping("/info-user")
	public ResponseEntity<String> consultarUser(@RequestBody Integer id) {
		try {			
			return new ResponseEntity<String>(demograficoBo.consultarUser(id), HttpStatus.CREATED);			
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	
	@PostMapping("/find-pacientes-filter")
	public ResponseEntity<String> findPacientesFilter(@RequestBody FilterPacienteDto dto){		
		try {
		
			return new ResponseEntity<String>(demograficoBo.getListKPacientes(dto), HttpStatus.CREATED);
			
		} catch (Exception e) {
			logger.error("Error general", e);
			return new ResponseEntity<String>("Error general.", HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		
	}
	
	
	
	
	
	

}
