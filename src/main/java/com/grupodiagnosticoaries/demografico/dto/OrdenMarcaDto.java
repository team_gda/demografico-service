package com.grupodiagnosticoaries.demografico.dto;

public class OrdenMarcaDto {

	private int kOrdenSucursal;
	private int cMarca;
	
	public OrdenMarcaDto(int kOrdenSucursal, int cMarca) {
		super();
		this.kOrdenSucursal = kOrdenSucursal;
		this.cMarca = cMarca;
	}

	public int getkOrdenSucursal() {
		return kOrdenSucursal;
	}

	public void setkOrdenSucursal(int kOrdenSucursal) {
		this.kOrdenSucursal = kOrdenSucursal;
	}

	public int getcMarca() {
		return cMarca;
	}

	public void setcMarca(int cMarca) {
		this.cMarca = cMarca;
	}
	
	
	
	
}
