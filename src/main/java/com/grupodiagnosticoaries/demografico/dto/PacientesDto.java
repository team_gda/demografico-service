package com.grupodiagnosticoaries.demografico.dto;

public class PacientesDto {
	private Integer consecutivo;
	private String ssucursal;
	private Integer uorden;
	private String snombresucursal;
	private String paciente;
	private String dnacimiento;
	
	
	
	public Integer getConsecutivo() {
		return consecutivo;
	}
	public void setConsecutivo(Integer consecutivo) {
		this.consecutivo = consecutivo;
	}
	public String getSsucursal() {
		return ssucursal;
	}
	public void setSsucursal(String ssucursal) {
		this.ssucursal = ssucursal;
	}
	public Integer getUorden() {
		return uorden;
	}
	public void setUorden(Integer uorden) {
		this.uorden = uorden;
	}
	public String getSnombresucursal() {
		return snombresucursal;
	}
	public void setSnombresucursal(String snombresucursal) {
		this.snombresucursal = snombresucursal;
	}
	public String getPaciente() {
		return paciente;
	}
	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}
	public String getDnacimiento() {
		return dnacimiento;
	}
	public void setDnacimiento(String dnacimiento) {
		this.dnacimiento = dnacimiento;
	}
	
	
	
}
