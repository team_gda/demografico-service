package com.grupodiagnosticoaries.demografico.dto;

public class CatExamenLabcoreDto {
	
	private Integer cexamenlabcoreexcepcion;
	private String scodigolis;
	private Integer cexamen;
	private Integer ntiporegistro;
	
	
	public CatExamenLabcoreDto(Integer cexamenlabcoreexcepcion, String scodigolis, Integer cexamen,
			Integer ntiporegistro) {
		super();
		this.cexamenlabcoreexcepcion = cexamenlabcoreexcepcion;
		this.scodigolis = scodigolis;
		this.cexamen = cexamen;
		this.ntiporegistro = ntiporegistro;
	}


	public Integer getCexamenlabcoreexcepcion() {
		return cexamenlabcoreexcepcion;
	}


	public void setCexamenlabcoreexcepcion(Integer cexamenlabcoreexcepcion) {
		this.cexamenlabcoreexcepcion = cexamenlabcoreexcepcion;
	}


	public String getScodigolis() {
		return scodigolis;
	}


	public void setScodigolis(String scodigolis) {
		this.scodigolis = scodigolis;
	}


	public Integer getCexamen() {
		return cexamen;
	}


	public void setCexamen(Integer cexamen) {
		this.cexamen = cexamen;
	}


	public Integer getNtiporegistro() {
		return ntiporegistro;
	}


	public void setNtiporegistro(Integer ntiporegistro) {
		this.ntiporegistro = ntiporegistro;
	}
	
	
	
	
	
}
