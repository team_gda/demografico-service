package com.grupodiagnosticoaries.demografico.dto;

public class PacienteInfoDto {
	private String paciente;
	private String telefono;
	private String sconvenio;
	
	
	
	public String getPaciente() {
		return paciente;
	}
	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getSconvenio() {
		return sconvenio;
	}
	public void setSconvenio(String sconvenio) {
		this.sconvenio = sconvenio;
	}
	
	
	
}
