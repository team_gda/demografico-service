package com.grupodiagnosticoaries.demografico.dto;

public class FilterPacienteDto {

	private String nombre;
	private String apPaterno;
	private String apMaterno;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApPaterno() {
		return apPaterno;
	}
	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}
	public String getApMaterno() {
		return apMaterno;
	}
	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}
	@Override
	public String toString() {
		return "FilterPacienteDto [nombre=" + nombre + ", apPaterno=" + apPaterno + ", apMaterno=" + apMaterno + "]";
	}
	
	
	
}
