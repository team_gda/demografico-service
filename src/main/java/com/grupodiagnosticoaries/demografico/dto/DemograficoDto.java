package com.grupodiagnosticoaries.demografico.dto;

import java.util.Date;
import java.util.List;

public class DemograficoDto {
	private String sNombre;
	private String sApellidoPaterno;
	private String sApellidoMaterno;
	private String sDireccion;
	private Integer cCodigoPostal;
	private Integer bSexo;
	private String sTelefono;
	private String sCorreoElectronico;
	private String dNacimiento;
	private String edad;
	private String sSucursal;
	private Integer cSucursal;
	private String sDireccionSucursal;
	private Integer kOrdenSucursal;
	private String sConvenio;
	private Integer cMarca;
	private String dRegistro;
	private String sNombreSucursal;
	private String sNombreMedico;
	private String sTelefonoSucursal;
	private Integer kpaciente;
	private List<ExamenDto> listExamenes;
	
	


	public DemograficoDto(String sNombre, String sApellidoPaterno, String sApellidoMaterno, String sDireccion,
			Integer cCodigoPostal, Integer bSexo, String sTelefono, String sCorreoElectronico, String dNacimiento,
			String edad, String sSucursal, Integer cSucursal, String sDireccionSucursal, Integer kOrdenSucursal,
			String sConvenio, Integer cMarca, String dRegistro, String sNombreSucursal, String sNombreMedico,
			String sTelefonoSucursal, Integer kpaciente) {
		super();
		this.sNombre = sNombre;
		this.sApellidoPaterno = sApellidoPaterno;
		this.sApellidoMaterno = sApellidoMaterno;
		this.sDireccion = sDireccion;
		this.cCodigoPostal = cCodigoPostal;
		this.bSexo = bSexo;
		this.sTelefono = sTelefono;
		this.sCorreoElectronico = sCorreoElectronico;
		this.dNacimiento = dNacimiento;
		this.edad = edad;
		this.sSucursal = sSucursal;
		this.cSucursal = cSucursal;
		this.sDireccionSucursal = sDireccionSucursal;
		this.kOrdenSucursal = kOrdenSucursal;
		this.sConvenio = sConvenio;
		this.cMarca = cMarca;
		this.dRegistro = dRegistro;
		this.sNombreSucursal = sNombreSucursal;
		this.sNombreMedico = sNombreMedico;
		this.sTelefonoSucursal = sTelefonoSucursal;
		this.kpaciente = kpaciente;
	}


	@Override
	public String toString() {
		return "DemograficoDto [sNombre=" + sNombre + ", sApellidoPaterno=" + sApellidoPaterno + ", sApellidoMaterno="
				+ sApellidoMaterno + ", sDireccion=" + sDireccion + ", cCodigoPostal=" + cCodigoPostal + ", bSexo="
				+ bSexo + ", sTelefono=" + sTelefono + ", sCorreoElectronico=" + sCorreoElectronico + ", dNacimiento="
				+ dNacimiento + ", edad=" + edad + ", sSucursal=" + sSucursal + ", cSucursal=" + cSucursal
				+ ", sDireccionSucursal=" + sDireccionSucursal + ", kOrdenSucursal=" + kOrdenSucursal + ", sConvenio="
				+ sConvenio + ", cMarca=" + cMarca + ", dRegistro=" + dRegistro + ", sNombreSucursal=" + sNombreSucursal
				+ ", sNombreMedico=" + sNombreMedico + ", sTelefonoSucursal=" + sTelefonoSucursal + ", listExamenes="
				+ listExamenes + "]";
	}

	
	

	public Integer getKpaciente() {
		return kpaciente;
	}


	public void setKpaciente(Integer kpaciente) {
		this.kpaciente = kpaciente;
	}


	public String getsNombre() {
		return sNombre;
	}
	public void setsNombre(String sNombre) {
		this.sNombre = sNombre;
	}
	public String getsApellidoPaterno() {
		return sApellidoPaterno;
	}
	public void setsApellidoPaterno(String sApellidoPaterno) {
		this.sApellidoPaterno = sApellidoPaterno;
	}
	public String getsApellidoMaterno() {
		return sApellidoMaterno;
	}
	public void setsApellidoMaterno(String sApellidoMaterno) {
		this.sApellidoMaterno = sApellidoMaterno;
	}
	public String getsDireccion() {
		return sDireccion;
	}
	public void setsDireccion(String sDireccion) {
		this.sDireccion = sDireccion;
	}
	public Integer getcCodigoPostal() {
		return cCodigoPostal;
	}
	public void setcCodigoPostal(Integer cCodigoPostal) {
		this.cCodigoPostal = cCodigoPostal;
	}
	public Integer getbSexo() {
		return bSexo;
	}
	public void setbSexo(Integer bSexo) {
		this.bSexo = bSexo;
	}
	public String getsTelefono() {
		return sTelefono;
	}
	public void setsTelefono(String sTelefono) {
		this.sTelefono = sTelefono;
	}
	public String getsCorreoElectronico() {
		return sCorreoElectronico;
	}
	public void setsCorreoElectronico(String sCorreoElectronico) {
		this.sCorreoElectronico = sCorreoElectronico;
	}
	public String getdNacimiento() {
		return dNacimiento;
	}
	public void setdNacimiento(String dNacimiento) {
		this.dNacimiento = dNacimiento;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	public String getsSucursal() {
		return sSucursal;
	}
	public void setsSucursal(String sSucursal) {
		this.sSucursal = sSucursal;
	}
	public String getsDireccionSucursal() {
		return sDireccionSucursal;
	}
	public void setsDireccionSucursal(String sDireccionSucursal) {
		this.sDireccionSucursal = sDireccionSucursal;
	}
	public Integer getkOrdenSucursal() {
		return kOrdenSucursal;
	}
	public void setkOrdenSucursal(Integer kOrdenSucursal) {
		this.kOrdenSucursal = kOrdenSucursal;
	}
	public String getsConvenio() {
		return sConvenio;
	}
	public void setsConvenio(String sConvenio) {
		this.sConvenio = sConvenio;
	}
	public List<ExamenDto> getListExamenes() {
		return listExamenes;
	}
	public void setListExamenes(List<ExamenDto> listExamenes) {
		this.listExamenes = listExamenes;
	}

	public Integer getcMarca() {
		return cMarca;
	}

	public void setcMarca(Integer cMarca) {
		this.cMarca = cMarca;
	}



	public String getdRegistro() {
		return dRegistro;
	}



	public void setdRegistro(String dRegistro) {
		this.dRegistro = dRegistro;
	}



	public String getsNombreSucursal() {
		return sNombreSucursal;
	}



	public void setsNombreSucursal(String sNombreSucursal) {
		this.sNombreSucursal = sNombreSucursal;
	}



	public String getsNombreMedico() {
		return sNombreMedico;
	}



	public void setsNombreMedico(String sNombreMedico) {
		this.sNombreMedico = sNombreMedico;
	}



	public String getsTelefonoSucursal() {
		return sTelefonoSucursal;
	}



	public void setsTelefonoSucursal(String sTelefonoSucursal) {
		this.sTelefonoSucursal = sTelefonoSucursal;
	}




	public Integer getcSucursal() {
		return cSucursal;
	}




	public void setcSucursal(Integer cSucursal) {
		this.cSucursal = cSucursal;
	}
	
	
	
}
