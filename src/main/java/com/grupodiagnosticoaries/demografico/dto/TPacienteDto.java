package com.grupodiagnosticoaries.demografico.dto;

public class TPacienteDto {
	private Integer kpaciente;
	private String snombre;
	private String sapellidopaterno;
	private String sapellidomaterno;
	private String dnacimiento;
	
	public TPacienteDto(Integer kpaciente, String snombre, String sapellidopaterno, String sapellidomaterno,
			String dnacimiento) {
		super();
		this.kpaciente = kpaciente;
		this.snombre = snombre;
		this.sapellidopaterno = sapellidopaterno;
		this.sapellidomaterno = sapellidomaterno;
		this.dnacimiento = dnacimiento;
	}

	public Integer getKpaciente() {
		return kpaciente;
	}

	public void setKpaciente(Integer kpaciente) {
		this.kpaciente = kpaciente;
	}

	public String getSnombre() {
		return snombre;
	}

	public void setSnombre(String snombre) {
		this.snombre = snombre;
	}

	public String getSapellidopaterno() {
		return sapellidopaterno;
	}

	public void setSapellidopaterno(String sapellidopaterno) {
		this.sapellidopaterno = sapellidopaterno;
	}

	public String getSapellidomaterno() {
		return sapellidomaterno;
	}

	public void setSapellidomaterno(String sapellidomaterno) {
		this.sapellidomaterno = sapellidomaterno;
	}

	public String getDnacimiento() {
		return dnacimiento;
	}

	public void setDnacimiento(String dnacimiento) {
		this.dnacimiento = dnacimiento;
	}
	
	
	
}
