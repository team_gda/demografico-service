package com.grupodiagnosticoaries.demografico.dto;

public class ExamenDto {
	
	private Integer cExamen;
	private String sExamen;
	private String scodigolis;
	private Integer cexamenproceso;
	
	public ExamenDto(Integer cExamen, String sExamen, String scodigolis, Integer cexamenproceso) {
		super();
		this.cExamen = cExamen;
		this.sExamen = sExamen;
		this.scodigolis = scodigolis;
		this.cexamenproceso = cexamenproceso;
	}
	public Integer getCexamenproceso() {
		return cexamenproceso;
	}
	public void setCexamenproceso(Integer cexamenproceso) {
		this.cexamenproceso = cexamenproceso;
	}

	public Integer getcExamen() {
		return cExamen;
	}
	public void setcExamen(Integer cExamen) {
		this.cExamen = cExamen;
	}
	public String getsExamen() {
		return sExamen;
	}
	public void setsExamen(String sExamen) {
		this.sExamen = sExamen;
	}

	public String getScodigolis() {
		return scodigolis;
	}

	public void setScodigolis(String scodigolis) {
		this.scodigolis = scodigolis;
	}

	
}
