package com.grupodiagnosticoaries.demografico.responses;

public class ResponseDemograficoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7952447986857101552L;
	
	private ResponseDemograficoDto responseDemograficoDto;
	
	
	

	public ResponseDemograficoException(String codigoTimbrado, String descripcionTimbrado) {
		super();
		this.responseDemograficoDto = new ResponseDemograficoDto();
		this.responseDemograficoDto.setCodigoTimbrado(codigoTimbrado);
		this.responseDemograficoDto.setDescripcionTimbrado(descripcionTimbrado);
	}

	public ResponseDemograficoDto getResponseTimbradoDto() {
		return responseDemograficoDto;
	}

	public void setResponseTimbradoDto(ResponseDemograficoDto responseDemograficoDto) {
		this.responseDemograficoDto = responseDemograficoDto;
	}
	

}
