package com.grupodiagnosticoaries.demografico.responses;

public class ResponseDemograficoDto {

	private String codigoTimbrado;
	private String descripcionTimbrado;
	
	public ResponseDemograficoDto() {
		super();
	}
	public ResponseDemograficoDto(String codigoTimbrado, String descripcionTimbrado) {
		super();
		this.codigoTimbrado = codigoTimbrado;
		this.descripcionTimbrado = descripcionTimbrado;
	}
	public String getCodigoTimbrado() {
		return codigoTimbrado;
	}
	public void setCodigoTimbrado(String codigoTimbrado) {
		this.codigoTimbrado = codigoTimbrado;
	}
	public String getDescripcionTimbrado() {
		return descripcionTimbrado;
	}
	public void setDescripcionTimbrado(String descripcionTimbrado) {
		this.descripcionTimbrado = descripcionTimbrado;
	}
	
	@Override
	public String toString() {
		return "ResponseDemograficoDto [codigoTimbrado=" + codigoTimbrado + ", descripcionTimbrado=" + descripcionTimbrado
				+ "]";
	}
	
	
}
