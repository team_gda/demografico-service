package com.grupodiagnosticoaries.demografico.bo;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.grupodiagnosticoaries.demografico.dao.inte.IDemograficoDao;
import com.grupodiagnosticoaries.demografico.dto.DemograficoDto;
import com.grupodiagnosticoaries.demografico.dto.ExamenDto;
import com.grupodiagnosticoaries.demografico.dto.FilterPacienteDto;
import com.grupodiagnosticoaries.demografico.dto.PacienteInfoDto;
import com.grupodiagnosticoaries.demografico.dto.PacientesDto;
import com.grupodiagnosticoaries.demografico.dto.TPacienteDto;
import com.grupodiagnosticoaries.demografico.responses.ResponseDemograficoException;

@Service
public class DemograficoBo {
	final static Logger logger = LogManager.getLogger(DemograficoBo.class);

	@Autowired
	private Environment env;
	
	@Autowired
	@Qualifier("demograficoDao")
	IDemograficoDao demograficoDao;

	/**
	 * Solicita el timbrado del XML y lo agrega al comprobante para finalmente
	 * devolver el XML timbrado.
	 * 
	 * @param xml
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws ResponseDemograficoException
	 * @throws JAXBException
	 */
	public DemograficoDto consultarDemografico(int kOrdenSucursal) throws IOException, ResponseDemograficoException {
		DemograficoDto demograficoDto = null;
//		if(cMarca != 7) {
			demograficoDto = demograficoDao.consultarDemograficoByOrden(kOrdenSucursal);
//		}else {
//			demograficoDto = demograficoDao.consultarDemograficoJenner(kOrdenSucursal);
//		}
		return demograficoDto;
	}

	public List<ExamenDto> selectAllExamenesByOrden(Integer kOrdenSucursal) {
		return demograficoDao.selectAllExamenesByOrden(kOrdenSucursal);
	}
	
	public List<ExamenDto> selectAllExamenesByCotizacion(Integer cotizacion){
		return demograficoDao.selectAllExamenesByCotizacion(cotizacion);
	}

	public DemograficoDto consultaDemograficoByCotizacion(int cotizacion) throws IOException, ResponseDemograficoException {
		DemograficoDto dto = null;
		dto = demograficoDao.consultarDemograficoByCotizacion(cotizacion);
		return dto;
	}
	
	public String consultarFechaRegistroCotizacion(Integer id){
		return demograficoDao.consultarFechaCotizacion(id);
	}
	
	public String consultarNombrePacienteById(Integer id){
		return demograficoDao.consultarNombrePacienteById(id);
	}
	
	public Integer consultarEstatusExamen(Integer consecutivo, Integer examen){
		return demograficoDao.consultarEstatusExamenByConsecutivoAndExamen(consecutivo, examen);
	}
	
	public String consultarNacimientoPacienteById(Integer id){
		
		Date nacimiento = demograficoDao.consultarNacimientoPacienteById(id);
		if(nacimiento!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			String fecha = sdf.format(nacimiento);
			return fecha;
		}else{
			return "";
		}
		
	}
	
	public String consultarNombreExamenProcesoById(Integer id){
		return demograficoDao.consultarNombreExamenProcesoById(id);
	}
	
	
	public String consultarIndicacionesByExamen(Integer examen){
		return demograficoDao.consultarIndicacionesExamen(examen);
	}
	
	public List<PacientesDto> getListPacientes(FilterPacienteDto dto){
		return demograficoDao.consultarPacientes(dto);
	}
	
	public String getListKPacientes(FilterPacienteDto dto){
		List<Integer> list = demograficoDao.consultarKPacientesByFilter(dto);
		if(list.size()>0){
			String kpacientes = "";
			for(int i=0;i<list.size();i++){
				if(i == (list.size()-1)){
					kpacientes+=list.get(i);
				}else{
					kpacientes+=list.get(i)+",";
				}
			}
			return kpacientes;
		}else{
			return "0";
		}
	}
	
	
	public PacienteInfoDto getInfoPacienteCotizacion(Integer id){
		return demograficoDao.consultarInfoPacienteByCotizacion(id);
	}
	
	public PacienteInfoDto getInfoPacienteConsecutivo(Integer id){
		return demograficoDao.consultarInfoPacienteByConsecutivo(id); 
	}
	
	public String consultarUser(Integer idUser){
		return demograficoDao.consultarUser(idUser);
	}
	
}
